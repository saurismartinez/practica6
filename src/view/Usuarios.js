import React, { useEffect } from "react";
import { Navegacion } from '../Components/Navegacion';
import { Button, Modal, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/Productos.css';
import { FaTrashAlt, FaEdit } from 'react-icons/fa';
import { useNavigate  } from "react-router-dom";

function Usuarios() {

  const navigate = useNavigate();

  const [correo_session, setCorreoSession] = React.useState("");
  const [pass_session, setPassSession]     = React.useState("");

  const [idUsuario, setId] = React.useState("");

  const [nombre, setNombre] = React.useState("");
  const [ap1, setAp1]       = React.useState("");
  const [ap2, setAp2]       = React.useState("");
  const [correo, setCorreo] = React.useState("");
  const [pass, setPass]     = React.useState("");

  let usuarios = JSON.parse(localStorage.getItem('array_usuarios'));

  if (usuarios === null) {
    usuarios = [];
  }

  const [show, setShow]         = React.useState(false);
  const [showEdit, setShowEdit] = React.useState(false);

  const handleHide = () => setShow(false);

  const handleHideEdit = () => setShowEdit(false);

  function modalAdd(){
    limpiarModal();
    setShow(true);
  }

  function addUsuario(event){
    event.preventDefault();   
    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_usuarios"));

    if(arreglo_localStorage == null) arreglo_localStorage = [];
    
    var array_newUsuario = {
        "nombre": nombre,
        "ap1":  ap1,
        "ap2": ap2,
        "correo": correo,
        "pass": pass
    };

    arreglo_localStorage.push(array_newUsuario);
    localStorage.setItem("array_usuarios", JSON.stringify(arreglo_localStorage));
    setShow(false);
    limpiarModal();
  }

  function deleteUsuario(correo) {
    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_usuarios"));
    arreglo_localStorage = arreglo_localStorage.filter(usuario => usuario.correo !== correo);
    localStorage.setItem('array_usuarios', JSON.stringify(arreglo_localStorage));
    alert("Usuario eliminado");
    window.location.reload(true);
  };

  function modalEdit (index){

    setId(index);

    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_usuarios"));
    setNombre(arreglo_localStorage[index].nombre);
    setAp1(arreglo_localStorage[index].ap1);
    setAp2(arreglo_localStorage[index].ap2);
    setCorreo(arreglo_localStorage[index].correo);
    setPass(arreglo_localStorage[index].pass);
    setShowEdit(true);
  }

  function updateUsuario() {

    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_usuarios"));

    arreglo_localStorage[idUsuario].nombre = nombre;
    arreglo_localStorage[idUsuario].ap1    = ap1;
    arreglo_localStorage[idUsuario].ap2    = ap2;
    arreglo_localStorage[idUsuario].correo = correo;
    arreglo_localStorage[idUsuario].pass   = pass;

    localStorage.setItem("array_usuarios", JSON.stringify(arreglo_localStorage));

    setShowEdit(false);
    window.location.reload(true);
  };

  function limpiarModal(){
    setNombre("");
    setAp1("");
    setAp2("");
    setCorreo("");
    setPass("");
  }

  useEffect(() => {
    setCorreoSession(localStorage.getItem("correo"));
    setPassSession(localStorage.getItem("pass"));
  }, [idUsuario, correo_session, pass_session]);

    //Validar sesión
    if (correo_session === "" || correo_session === null || pass_session === "" || pass_session === null) {
      navigate("/");
    }

  return (

    <div>
      <Navegacion></Navegacion>

      <div class="container">

        <div id="control_citas" class="container-fluid ">
          <div class='row' style={{width: '90%'}}>
            <div class="col-sm-12">

              <h2 style={{textAlign: 'center'}}>Control de usuarios</h2>

              <div style={{margin: '1rem',  justifyContent: 'right', display: 'flex'}}>
                <button class="btn btn-success" onClick={modalAdd}>Agregar (+)</button>
              </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-hover">
                    <tbody>
                      <tr style={{textAlign: 'center'}}>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Contraseña</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                      </tr>
                      {usuarios.map((usuario, index) => (
                      <tr style={{textAlign: 'center'}}>
                        <td>{index+1}</td>
                        <td>{usuario.nombre} {usuario.ap1} {usuario.ap2}</td>
                        <td>{usuario.correo}</td>
                        <td>{usuario.pass}</td>
                        <td>Activo</td>
                        <td>
                        <div style={{display: 'flex', justifyContent: 'center'}}>
                          <button class="btn btn-warning" style={{margin: 'auto'}} onClick={()=>modalEdit(index)}><FaEdit /></button>
                          <button class="btn btn-danger" style={{margin: 'auto'}} onClick={()=>deleteUsuario(usuario.correo)}><FaTrashAlt /></button>
                        </div>
                        </td>
                      </tr>
                      ))}
                    </tbody>
                  </table>
                </div>

            </div>
          </div>
        </div>

      </div>

      <Modal show={show}>
        <Modal.Header>
          <Modal.Title>Agregar usuario</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <>    
          <Form>
            <Form.Group controlId="formNombre">
              <Form.Label>Nombre</Form.Label>
              <Form.Control type="text" placeholder="Nombre" value={nombre} onChange={e => setNombre(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formAp1">
              <Form.Label>Apellido paterno</Form.Label>
              <Form.Control type="text" placeholder="Apellido paterno" value={ap1} onChange={e => setAp1(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formAp2">
              <Form.Label>Apellido materno</Form.Label>
              <Form.Control type="text" placeholder="Apellido materno" value={ap2} onChange={e => setAp2(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formCorreo">
              <Form.Label>Correo</Form.Label>
              <Form.Control type="email" placeholder="Correo" value={correo} onChange={e => setCorreo(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formPass">
              <Form.Label>Contraseña</Form.Label>
              <Form.Control type="password" placeholder="Contraseña" value={pass} onChange={e => setPass(e.target.value)}/>
            </Form.Group>
          </Form>
          </>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleHide}>Cancelar</Button>
          <Button variant="success" onClick={addUsuario}>Guardar</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showEdit}>
        <Modal.Header>
          <Modal.Title>Editar producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <>    
          <Form>
            <Form.Group controlId="formNombre">
              <Form.Label>Nombre</Form.Label>
              <Form.Control type="text" placeholder="Nombre" value={nombre} onChange={e => setNombre(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formAp1">
              <Form.Label>Apellido paterno</Form.Label>
              <Form.Control type="text" placeholder="Apellido paterno" value={ap1} onChange={e => setAp1(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formAp2">
              <Form.Label>Apellido materno</Form.Label>
              <Form.Control type="text" placeholder="Apellido materno" value={ap2} onChange={e => setAp2(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formCorreo">
              <Form.Label>Correo</Form.Label>
              <Form.Control type="email" placeholder="Correo" value={correo} onChange={e => setCorreo(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formPass">
              <Form.Label>Contraseña</Form.Label>
              <Form.Control type="password" placeholder="Contraseña" value={pass} onChange={e => setPass(e.target.value)}/>
            </Form.Group>
          </Form>
          </>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleHideEdit}>Cancelar</Button>
          <Button variant="success" onClick={()=>updateUsuario()}>Editar</Button>
        </Modal.Footer>
      </Modal>

    </div>


    );
  }

export {Usuarios};

