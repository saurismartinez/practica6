import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/App.css'
import {Login} from '../view/Login.js'
import {Home } from '../view/Home.js'
import {Productos } from '../view/Productos.js'
import {Usuarios } from '../view/Usuarios.js'
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {

  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Login/>} />
      <Route  path="/Home" element={<Home/>} />
      <Route  path="Productos" element={<Productos/>} />
      <Route  path="/Usuarios" element={<Usuarios/>} />
    </Routes>
  </BrowserRouter>
  );
}

export default App;
