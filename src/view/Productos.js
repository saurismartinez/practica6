import React, { useEffect } from "react";
import { Navegacion } from '../Components/Navegacion';
import { Button, Modal, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/Productos.css';
import { FaTrashAlt, FaEdit } from 'react-icons/fa';
import { useNavigate  } from "react-router-dom";

function Productos() {

  const navigate = useNavigate();

  const [correo, setCorreo] = React.useState("");
  const [pass, setPass]     = React.useState("");

  const [idProducto, setId] = React.useState("");

  const [nombre, setNombre] = React.useState("");
  const [desc, setDesc]     = React.useState("");
  const [precio, setPre]    = React.useState("");
  const [cant, setCant]     = React.useState("");

  let productos = JSON.parse(localStorage.getItem('array_productos'));

  if (productos === null) {
      productos = [];
  }

  const [show, setShow]         = React.useState(false);
  const [showEdit, setShowEdit] = React.useState(false);

  const handleHide = () => setShow(false);

  const handleHideEdit = () => setShowEdit(false);

  function modalAdd(){
    limpiarModal();
    setShow(true);
  }

  function addProducto(event){
    event.preventDefault();   
    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_productos"));

    if(arreglo_localStorage == null) arreglo_localStorage = [];
    
    var array_newProducto = {
        "nombre": nombre,
        "desc": desc,
        "precio": precio,
        "cant": cant
    };

    arreglo_localStorage.push(array_newProducto);
    localStorage.setItem("array_productos", JSON.stringify(arreglo_localStorage));
    setShow(false);
    limpiarModal();
  }

  function deleteProducto (nombre) {
    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_productos"));
    arreglo_localStorage = arreglo_localStorage.filter(producto => producto.nombre !== nombre);
    localStorage.setItem('array_productos', JSON.stringify(arreglo_localStorage));
    alert("Producto eliminado");
    window.location.reload(true);
  };

  function modalEdit (index){

    setId(index);

    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_productos"));
    setNombre(arreglo_localStorage[index].nombre);
    setDesc(arreglo_localStorage[index].desc);
    setPre(arreglo_localStorage[index].precio);
    setCant(arreglo_localStorage[index].cant);
    setShowEdit(true);
  }

  function updateProducto () {

    var arreglo_localStorage = JSON.parse(localStorage.getItem("array_productos"));

    arreglo_localStorage[idProducto].nombre = nombre;
    arreglo_localStorage[idProducto].desc   = desc;
    arreglo_localStorage[idProducto].precio = precio;
    arreglo_localStorage[idProducto].cant   = cant;

    localStorage.setItem("array_productos", JSON.stringify(arreglo_localStorage));

    setShowEdit(false);
    window.location.reload(true);
  };

  function limpiarModal(){
    setNombre("");
    setDesc("");
    setPre("");
    setCant("");
  }

  useEffect(() => {
    setCorreo(localStorage.getItem("correo"));
    setPass(localStorage.getItem("pass"));
  }, [idProducto, correo, pass]);

  //Validar sesión
  if (correo === "" || correo === null || pass === "" || pass === null) {
    navigate("/");
  }

  return (

    <div>
      <Navegacion></Navegacion>

      <div className="container">

        <div id="control_citas" className="container-fluid ">
          <div className='row' style={{width: '90%'}}>
            <div className="col-sm-12">

              <h2 style={{textAlign: 'center'}}>Control de productos</h2>

              <div style={{margin: '1rem',  justifyContent: 'right', display: 'flex'}}>
                <button className="btn btn-success" onClick={modalAdd}>Agregar (+)</button>
              </div>

                <div className="table-responsive">
                  <table className="table table-bordered table-hover">
                    <tbody>
                      <tr style={{textAlign: 'center'}}>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                      </tr>
                      {productos.map((producto, index) => (
                      <tr style={{textAlign: 'center'}}>
                        <td>{index+1}</td>
                        <td>{producto.nombre}</td>
                        <td>{producto.desc}</td>
                        <td>$ {producto.precio} MXM</td>
                        <td>{producto.cant}</td>
                        <td>Activo</td>
                        <td>
                        <div style={{display: 'flex', justifyContent: 'center'}}>
                          <button className="btn btn-warning" style={{margin: 'auto'}} onClick={()=>modalEdit(index)}><FaEdit /></button>
                          <button className="btn btn-danger" style={{margin: 'auto'}} onClick={()=>deleteProducto(producto.nombre)}><FaTrashAlt /></button>
                        </div>
                        </td>
                      </tr>
                      ))}
                    </tbody>
                  </table>
                </div>

            </div>
          </div>
        </div>

      </div>

      <Modal show={show}>
        <Modal.Header>
          <Modal.Title>Agregar producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <>    
          <Form>
            <Form.Group controlId="formNombre">
              <Form.Label>Nombre</Form.Label>
              <Form.Control type="text" placeholder="Nombre" value={nombre} onChange={e => setNombre(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formDesc">
              <Form.Label>Descripción</Form.Label>
              <Form.Control type="text" placeholder="Descripción" value={desc} onChange={e => setDesc(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formPrecio">
              <Form.Label>Precio</Form.Label>
              <Form.Control type="number" placeholder="Precio" value={precio} onChange={e => setPre(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formCant">
              <Form.Label>Cantidad</Form.Label>
              <Form.Control type="number" placeholder="Cantidad" value={cant} onChange={e => setCant(e.target.value)}/>
            </Form.Group>
          </Form>
          </>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleHide}>Cancelar</Button>
          <Button variant="success" onClick={addProducto}>Guardar</Button>
        </Modal.Footer>
      </Modal>

      <Modal show={showEdit}>
        <Modal.Header>
          <Modal.Title>Editar producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <>    
          <Form>
            <Form.Group controlId="formNombre">
              <Form.Label>Nombre</Form.Label>
              <Form.Control type="text" placeholder="Nombre" value={nombre} onChange={e => setNombre(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formDesc">
              <Form.Label>Descripción</Form.Label>
              <Form.Control type="text" placeholder="Descripción" value={desc} onChange={e => setDesc(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formPrecio">
              <Form.Label>Precio</Form.Label>
              <Form.Control type="number" placeholder="Precio" value={precio} onChange={e => setPre(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formCant">
              <Form.Label>Cantidad</Form.Label>
              <Form.Control type="number" placeholder="Cantidad" value={cant} onChange={e => setCant(e.target.value)}/>
            </Form.Group>
          </Form>
          </>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleHideEdit}>Cancelar</Button>
          <Button variant="success" onClick={()=>updateProducto()}>Editar</Button>
        </Modal.Footer>
      </Modal>

    </div>



    );
  }

export {Productos};

