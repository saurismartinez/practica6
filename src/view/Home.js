import React, { useEffect } from 'react';
import { Navegacion } from '../Components/Navegacion';
import { useNavigate  } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/Home.css'


function Home() {

  const navigate = useNavigate();

  const [correo, setCorreo] = React.useState("");
  const [pass, setPass]     = React.useState("");

  useEffect(() => {
      setCorreo(localStorage.getItem("correo"));
      setPass(localStorage.getItem("pass"));
  }, [correo, pass]);

  if (correo === "" || correo === null || pass === "" || pass === null) {
    navigate("/");
  }

  document.body.className="fondo";

  return (

    <div>
      <Navegacion></Navegacion>
      <h1>Bienvenido {correo}</h1>
    </div>

  );
}

export {Home};

