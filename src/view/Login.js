import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import '../css/Login.css';
import { Link, useNavigate  } from "react-router-dom";
import { Button} from "react-bootstrap";

function Login() {

  const navigate = useNavigate();

  const [correo, setCorreo]   = React.useState("");
  const [pass, setPass]       = React.useState("");

  function validar(){
    if(correo === "admin@barberyshop.com.mx" && pass === "admin1234*"){
      localStorage.setItem("correo", correo);
      localStorage.setItem("pass", pass);
      navigate("/Home");
    }else {
      alert("Usuario o contraseña incorrecto");
    }
  }

  document.body.className="fondologin";
  return (
    
 <div>
        <meta charSet="UTF-8" />
        <title>Login-session</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:500&display=swap" rel="stylesheet" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
        {/* font awesome */}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <div className="login">
          <h4 style={{textAlign: 'center', color: 'white'}}>TheBarberyShop</h4>
          <form  className="demo-form" data-parsley-validate="true" action="validar()">
            <label><i className="fa fa-user" />&nbsp; Correo</label>
            <input type="email" name="email" data-parsley-trigger="changes" required value={correo} onChange={e => setCorreo(e.target.value)}/>
            <label><i className="fa fa-lock" />&nbsp; Contraseña</label>
            <input type="password" name="password" required value={pass} onChange={e => setPass(e.target.value)}/>
            <br></br>
            <br></br>
            <br></br>
            <Button className="btn cssBtn"  role="button" onClick={()=>validar()}>Iniciar sesión</Button><hr />
            
          </form>
        </div>
      </div>
    );
  }
export {Login};

