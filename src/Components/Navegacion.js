import React from 'react';
import { Link } from "react-router-dom";
import '../css/App.css'


function Navegacion() {

  function logout(){
    localStorage.removeItem('correo');
    localStorage.removeItem('pass');
  }
  return (
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <Link className="navbar-brand" to="/Home">The Barbery Shop</Link>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="mynavbar">
          <ul class="navbar-nav me-auto">
            <li class="nav-item">
            <Link className="nav-link" to="/Productos">Productos</Link>
            </li>
            <li class="nav-item">
            <Link className="nav-link" to="/Usuarios">Usuarios</Link>
            </li>
            <li className="nav-item" style={{position: 'absolute', right: '20px'}}>
            <Link className="nav-link" to="/" onClick={()=>logout()}>Cerrar sesión</Link>
            </li>
          </ul>
        </div>
    </nav>
  );
}

export {Navegacion};

